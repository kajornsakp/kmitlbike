//
//  FindViewController.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 5/27/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import UIKit
import GoogleMaps

import SwiftEventBus
//API key AIzaSyBQWGJ4MdB4mwOuYVueu_lV0DKZE4CIFik
//IC location : 13.730056, 100.775434
class FindViewController: UIViewController,GMSMapViewDelegate{

    var bikeList :[GMSMarker] = [GMSMarker]()
    var camera = GMSCameraPosition()
    
    @IBOutlet weak var mapView: GMSMapView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        SwiftEventBus.onMainThread(self, name: "onFindBikeSuccess"){
            result in
            let bikeList : [FoundBike]  = result.object as! [FoundBike]
            for i in bikeList{
                
                let markerIcon = UIImage(named: "bike_marker")
                let marker1 = GMSMarker()
                marker1.appearAnimation = kGMSMarkerAnimationPop
                marker1.position = i.getCoordinate()
                marker1.title = i.getBikeName()
                marker1.snippet = i.getBikeMac()
                marker1.icon = markerIcon
                marker1.map = self.mapView
            }
            
        }
        
        initMap()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        FindServiceManager.sharedManager.findAllBike()
    }

    func initMap(){
        
        let currentLocation = LocationService.sharedInstance.lastLocation?.coordinate
        
        self.camera = GMSCameraPosition.cameraWithTarget(currentLocation!, zoom: 15)

        self.mapView.camera = self.camera
        self.mapView.myLocationEnabled = true
        self.mapView.settings.myLocationButton = true
        self.mapView.delegate = self
        
        
        
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        self.mapView.clear()
    }
    func mapView(mapView: GMSMapView, didTapAtCoordinate coordinate: CLLocationCoordinate2D) {
        print("User tapped at coordinate\(coordinate.latitude) : \(coordinate.longitude)")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

