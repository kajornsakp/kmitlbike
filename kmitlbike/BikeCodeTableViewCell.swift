//
//  BikeCodeTableViewCell.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 11/9/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import UIKit

class BikeCodeTableViewCell: UITableViewCell {

    @IBOutlet weak var bikeLabel : UILabel!
    @IBOutlet weak var bikeUnlock : UILabel!
    
    var bikeCode : bikeCodeObject!{
        didSet{
            self.bikeLabel.text = bikeCode.bikeName
        }
    }
    static let identifier = "BikeCodeTableViewCell"
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
