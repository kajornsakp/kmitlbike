//
//  DoubleConfig.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 7/10/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import Foundation


extension Double{
    
    var roundedTwoDigit:Double{
        
        return Double(round(100*self)/100)
        
    }
}