//
//  HomeViewController.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 5/27/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import UIKit
import Presentr
import SwiftEventBus
class HomeViewController: UIViewController,TutorialDelegate ,BikeCodeDelegate,PassCodeDelegate{
    
    @IBOutlet weak var borrowBikeButton: UIButton!
   
    @IBOutlet weak var instructionText: UILabel!
    
    var bike:Bike?
    var bikeCode : bikeCodeObject?
    
    let presenter: Presentr = {
        let presenter = Presentr(presentationType: .Popup)
        presenter.roundCorners = true
        presenter.transitionType = .CoverHorizontalFromRight // Optional
        return presenter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        checkIsRiding()
        
        SwiftEventBus.onMainThread(self, name: "onResumeRidingBike"){
            result in
            
            self.bike = result.object as! Bike
            self.performSegueWithIdentifier("isRidingSegue", sender: self)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
    }

    
    @IBAction func borrowPressed(sender: AnyObject) {
        showTutorial()
    }
    

    func gotoBorrowPage(){
        //self.performSegueWithIdentifier("borrowPressed", sender: self)
    }

    func showTutorial(){
        var popover = TutorialVC(nibName: "TutorialVC", bundle: nil)
        popover.delegate = self
        customPresentViewController(presenter, viewController: popover, animated: true, completion: nil)
    }
    
    func didTapButton(){
        //gotoBorrowPage()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("BikeCodeViewController") as! BikeCodeViewController
        vc.delegate = self
        customPresentViewController(presenter, viewController: vc, animated: true, completion: nil)
    }
    
    func checkIsRiding(){
        StatusServiceManager.sharedManager.getStatus()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "isRidingSegue" {
            var destinationVC = segue.destinationViewController as! ReturnViewController
            destinationVC.bike = self.bike
            
        }
        else if segue.identifier == "borrowCodeSuccess"{
            let vc = segue.destinationViewController as! ReturnViewController
            vc.bike = Bike()
            vc.bikeCode = self.bikeCode!
        }
    }
    
    func didTapBikeCode(bikeCode: bikeCodeObject) {
        let alertPresenter: Presentr = {
            let presenter = Presentr(presentationType: .Alert)
            presenter.roundCorners = true
            presenter.transitionType = .CoverHorizontalFromRight
            return presenter
        }()
        self.bikeCode = bikeCode
        var vc = ShowPassCodeViewController(nibName: "ShowPassCodeViewController", bundle: nil) as ShowPassCodeViewController
        vc.bikeCode = bikeCode
        vc.delegate = self
        customPresentViewController(alertPresenter, viewController: vc, animated: true, completion: nil)
    }
    
    func didDissmissPasscode(bikeCode: bikeCodeObject) {
        BorrowServiceManager.sharedManager.borrowBikeCode(bikeCode.bikeMac){
            (result,error) in
            if error == nil,let response = result! as String?{
                if(response == "success"){
                    self.performSegueWithIdentifier("borrowCodeSuccess", sender: nil)
                }
            }
            else{
                self.showAlertWithMessage("can't borrow")
            }
        }
    }
    
}

