//
//  HistoryViewController.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 5/27/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import UIKit
import SwiftEventBus

class HistoryViewController : UIViewController,UITableViewDataSource,UITableViewDelegate{
    
    
    @IBOutlet var tableView: UITableView!
    
    var bikeHistoryArray : [BikeHistory] = []
    var selectedCell :Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerNib(UINib.init(nibName: "HistoryCell", bundle: nil), forCellReuseIdentifier: "historyCell")
        SwiftEventBus.onMainThread(self, name: "onGetUsedBikeSuccess"){
            result in
            self.bikeHistoryArray = result.object as! [BikeHistory]
            self.bikeHistoryArray = self.bikeHistoryArray.sort{ (a, b) -> Bool in
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateStyle = .MediumStyle
                dateFormatter.timeZone = NSTimeZone(name: "UTC")
                let date = dateFormatter.dateFromString(a.borrow_date)
                let date2 = dateFormatter.dateFromString(b.borrow_date)
                return date?.earlierDate(date2!) != date
            }
            self.tableView.reloadData()
        }
        
        SwiftEventBus.onMainThread(self, name: "onGetUsedBikeFailure"){
            notification in
            print(notification)
            
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        callGetUsedBike()
    }
    
    func callGetUsedBike(){
        HistoryServiceManager.sharedManager.getUsedBike()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bikeHistoryArray.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("historyCell") as! HistoryTableViewCell
        let bikeHistory = self.bikeHistoryArray[indexPath.row]
        cell.durationLabel.text = bikeHistory.total_time
        cell.distanceLabel.text = "\(bikeHistory.total_distance) km"
        cell.dateLabel.text = bikeHistory.borrow_date
        cell.timeLabel.text = bikeHistory.borrow_time
      
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("Row \(indexPath.row) selected")
        self.selectedCell = indexPath.row
        
        self.performSegueWithIdentifier("chooseUsedBike", sender: self)
        
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: CGRectZero)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "chooseUsedBike" {
            var vc = segue.destinationViewController as! SummaryViewController
            vc.bikeHistory = bikeHistoryArray[self.selectedCell!]
        }
    }
    
    
}
