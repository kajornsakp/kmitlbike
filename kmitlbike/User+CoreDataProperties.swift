//
//  User+CoreDataProperties.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 7/12/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension User {

    @NSManaged var fullName: String?
    @NSManaged var isLogin: NSNumber?
    @NSManaged var ridingBike: NSNumber?
    @NSManaged var token: String?
    @NSManaged var username: String?
    
}
