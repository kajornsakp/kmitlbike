//
//  BikeModel.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 6/25/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import Foundation
import GoogleMaps
class Bike {
    var totalTime : String
    var timeSec : Int
    var distanceKm : Double
    var totalDistance : String?
    var routeLine  : [RoutePoint]?
    
    init()
    {
        totalTime = "00:00:00"
        totalDistance = "0.0"
        timeSec = 0
        distanceKm = 0
        routeLine = []
    }
    
    init(totaltime:String,totaldistance:String,routeLine:[RoutePoint]){
        self.totalTime = totaltime
        self.totalDistance = totaldistance
        self.routeLine = routeLine
        self.distanceKm = Double(totaldistance)!
        
        var timeArr = totaltime.characters.split{ $0 == ":"}.map{(x)->Int in return Int(String(x))!}
        var hour = timeArr[0]*3600
        var minute = timeArr[1]*60
        var sec = timeArr[2]
        self.timeSec = hour+minute+sec
        
    }
    
    func getSecFromString(time : String)->Int{
        
        var timeArr = time.characters.split{ $0 == ":"}.map{(x)->Int in return Int(String(x))!}
        var hour = timeArr[0]*3600
        var minute = timeArr[1]*60
        var sec = timeArr[2]
        
        return hour+minute+sec
    }
    func getTime() -> String {
        let hours = self.timeSec / 3600
        let minutes = self.timeSec / 60 % 60
        let seconds = self.timeSec % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    
   
    func incrementSecond()->Void {
        self.timeSec += 1
    }
    
}
