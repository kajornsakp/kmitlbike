//
//  HistoryServiceManager.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 7/2/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import Foundation
import SwiftEventBus
import Alamofire
import CoreLocation
class HistoryServiceManager {
    
    static let sharedManager = HistoryServiceManager()
    
    private var url = "\(BASE_URL)/api/v1/user/history/"
    func getUsedBike(){
        let header = ["AUTHORIZATION":User.getToken()]
        Alamofire.request(.GET,url, headers : header).responseJSON{ response in
            if(response.result.isSuccess){
                
                var bikeHistoryArray : [BikeHistory] = []
                let result = response.result.value as! NSDictionary
                if result["result"] as! String != "success"{
                    SwiftEventBus.post("onGetUsedBikeFailure",sender:"\(result["result"])")
                }
                let resultArray = result["history_list"] as! NSArray
                for dic in resultArray {
                    let bikeHistoryDict = dic as! NSDictionary
                    let bike_name = bikeHistoryDict["bike_name"] as! String
                    let borrow_date = bikeHistoryDict["borrow_date"] as! String
                    let borrow_time = bikeHistoryDict["borrow_time"] as! String
                    let total_time = bikeHistoryDict["total_time"] as! String
                    let total_distance = bikeHistoryDict["total_distance"] as! String
                    let routeLine = bikeHistoryDict["route_line"] as! NSArray
                    var route_line : [RoutePoint] = []
                    for latlng in routeLine{
                        var latlngDict = latlng as! NSDictionary
                        var lat = latlngDict["lat"] as! Double
                        var lng = latlngDict["lng"] as! Double
                        var time = latlngDict["time"] as! NSTimeInterval
                        var coordinate = RoutePoint(lat: lat, lng: lng, time: time)
                        //TODO : get time from route_line
                        route_line.append(coordinate)
                    }
                    var bikeDict = Dictionary<String,AnyObject>()
                    bikeDict["bike_name"] = bike_name
                    bikeDict["borrow_date"] = borrow_date
                    bikeDict["borrow_time"] = borrow_time
                    bikeDict["route_line"] = route_line
                    
                    bikeDict["total_time"] = total_time
                    bikeDict["total_distance"] = total_distance
                    let bikeHistoryObject = BikeHistory(dic: bikeDict)
                    bikeHistoryArray.append(bikeHistoryObject)
                }
                SwiftEventBus.post("onGetUsedBikeSuccess",sender:bikeHistoryArray)
            }else{
                SwiftEventBus.post("onGetUsedBikeFailure",sender:"\(response.result.error)")
            }
            
            
        }
        
    }
    
}