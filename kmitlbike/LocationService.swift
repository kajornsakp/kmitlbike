//
//  LocationService.swift
//  realmDemo
//
//  Created by Kajornsak Peerapathananont on 7/27/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import Foundation
import CoreLocation


protocol LocationServiceDelegate : class
{
    func tracingLocation(currentLocation: CLLocation)
    func tracingLocationDidFailWithError(error: NSError)
}


class LocationService : NSObject,CLLocationManagerDelegate{
    
    static let sharedInstance = LocationService()
    
    var locationManager : CLLocationManager?
    var lastLocation : CLLocation?
    weak var delegate :LocationServiceDelegate?
    
    override init(){
        super.init()
        self.locationManager = CLLocationManager()
        guard let locationManager = self.locationManager else{
            return
        }
        
        if CLLocationManager.authorizationStatus() == .NotDetermined {
            
            locationManager.requestWhenInUseAuthorization()
        }
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest // The accuracy of the location data
        locationManager.distanceFilter = 5
        locationManager.delegate = self
    }
    
    
    func startUpdatingLocation(){
        self.locationManager?.startUpdatingLocation()
    }
    
    func stopUpdatingLocation(){
        self.locationManager?.stopUpdatingLocation()
    }
    
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else{
            return
        }
        
        //for filter wrong location
        let age = -(location.timestamp.timeIntervalSinceNow)
        if(age > 120){
            return
        }
        if(location.horizontalAccuracy < 0){
            return
        }
        
        self.lastLocation = location
        updateLocation(location)
        
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        
        // do on error
        updateLocationDidFailWithError(error)
    }
    
    
    private func updateLocation(currentLocation: CLLocation){
        
        guard let delegate = self.delegate else {
            return
        }
        
        delegate.tracingLocation(currentLocation)
    }
    
    private func updateLocationDidFailWithError(error: NSError) {
        
        guard let delegate = self.delegate else {
            return
        }
        
        delegate.tracingLocationDidFailWithError(error)
    }
    
    
    
}
