//
//  LoginViewController.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 6/12/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import UIKit
import SwiftEventBus

class LoginViewController : UIViewController{
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    var activityIndicator = UIActivityIndicatorView()
    var keyboardAppear = false
    var username:String?
    var password:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)

        print("this is login screen")
        
        SwiftEventBus.onMainThread(self, name: "onLoginSuccess"){ result in
            self.resetLoginButton()
            self.gotoHomeViewController()
            self.usernameTextField.text = ""
            self.passwordTextField.text = ""
        }

        SwiftEventBus.onMainThread(self, name: "onLoginFailure"){result in
            print("result :\(result.object)")
            self.loginButton.shake()
            self.resetLoginButton()
        }
        
        SwiftEventBus.onMainThread(self, name: "onLoginFirstTime"){result in
            self.resetLoginButton()
            print("first login")
            self.gotoSignupViewController()
        }
        
        SwiftEventBus.onMainThread(self, name: "onNetworkServiceUnavailable"){
            result in
            self.showAlertWithMessage("Network is unvailable now, please try again later.")
            self.resetLoginButton()
        }
        SwiftEventBus.onMainThread(self, name: "onLoginDenied"){
            result in
            self.showAlertWithMessage("Incorrect username/password, please try again.")
            self.resetLoginButton()
        }
        

        
        
        
        
    }
    func gotoSignupViewController(){
        self.performSegueWithIdentifier("firstTimeSegue", sender: self)
    }
    
    func gotoHomeViewController(){
        self.performSegueWithIdentifier("loginSuccessSegue", sender: self)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if(!self.keyboardAppear){
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size {
            self.view.frame.origin.y = -keyboardSize.height
            self.keyboardAppear = true
        }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if(keyboardAppear){
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size {
            self.view.frame.origin.y += keyboardSize.height
            self.keyboardAppear = false
        }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func resetLoginButton(){
        self.activityIndicator.stopAnimating()
        self.loginButton.setTitle("Login", forState: UIControlState.Normal)
    }
    @IBAction func onLoginButtonPressed(sender: AnyObject) {
        self.username = self.usernameTextField.text
        self.password = self.passwordTextField.text
        print("username from text : \(self.username)")
        User.setUsername(self.username!)
        LoginServiceManager.getInstance().login(self.username!, password: self.password!)
        self.loginButton.addSubview(self.activityIndicator)
        self.activityIndicator.frame = self.loginButton.bounds
        self.loginButton.setTitle("", forState: UIControlState.Normal)
        self.activityIndicator.startAnimating()
        
    }
    
    @IBAction func onBackButtonPressed(sender: AnyObject) {
        User.clear()
        self.dismissViewControllerAnimated(true,completion: nil)
    }
    
    
    
}