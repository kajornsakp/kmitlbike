//
//  SummaryBikeNameCell.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 6/28/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import UIKit

class SummaryBikeNameCell: UITableViewCell {

    @IBOutlet weak var bikeNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        
    }
    
}