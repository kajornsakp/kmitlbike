//
//  StringConfig.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 7/10/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import Foundation

extension String {
    func toDouble() -> Double? {
        return NSNumberFormatter().numberFromString(self)?.doubleValue
    }
}

extension String {
    func sha1() -> String {
        let data = self.dataUsingEncoding(NSUTF8StringEncoding)!
        var digest = [UInt8](count:Int(CC_SHA1_DIGEST_LENGTH), repeatedValue: 0)
        CC_SHA1(data.bytes, CC_LONG(data.length), &digest)
        let hexBytes = digest.map { String(format: "%02hhx", $0) }
        return hexBytes.joinWithSeparator("")
    }
}



extension String{
    func sumHex(str :String)->Int{
        var sum = 0
        for i in str.characters{
            let ii = String(i)
            if sum < 62 {
                sum += hexToInt(ii)
            }else{
                sum = sumDigit(sum)
                sum += hexToInt(ii)
            }
        }
        return sum
    }
    func sumDigit(digit : Int)->Int{
        return (digit % 9 == 0 && digit != 0) ? 9 : digit % 9
    }
    
    func hexToInt(character : String)->Int{
        return Int(character, radix: 16)!
    }
    
    func mapped(index : Int)->String{
        let mapArray = ["l","9","p","a","q","y","m","e","n","W","k","5","6","N","P","h","C","Z","b","r","3","7","v","i","d","H","Y","c","V","s","4","j","S","O","x","R","T","L","2","1","G","J","u","g","E","f","w","B","z","8","K","0","M","o","F","D","X","A","Q","I","U","t"]
        return mapArray[index]
    }
    
    func shaToKey()->String{
        let str = self
        var keyOut = ""
        let first = str.startIndex.advancedBy(0)..<str.startIndex.advancedBy(10)
        let second = str.startIndex.advancedBy(10)..<str.startIndex.advancedBy(20)
        let third = str.startIndex.advancedBy(20)..<str.startIndex.advancedBy(30)
        let fourth = str.startIndex.advancedBy(30)..<str.startIndex.advancedBy(40)
        keyOut = mapped(sumHex(str[first])) + mapped(sumHex(str[second])) + mapped(sumHex(str[third])) + mapped(sumHex(str[fourth]))
        return keyOut
    }
    
    func removeColon()->String{
        return self.stringByReplacingOccurrencesOfString(":", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
    }
}