//
//  ProfileGenderTableViewCell.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 10/11/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import UIKit

class ProfileGenderTableViewCell: UITableViewCell {

    @IBOutlet weak var maleImageView: UIImageView!
    
    @IBOutlet weak var maleLabel: UILabel!
    
    
    @IBOutlet weak var femaleImageView: UIImageView!
    
    @IBOutlet weak var femaleLabel: UILabel!
    
    @IBOutlet weak var maleFemaleImageView: UIImageView!
    
    @IBOutlet weak var maleFemaleLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    var genderType:Int!{
        didSet{
            switch genderType {
            case 1:
                maleImageView.image = UIImage(named: "male_selected")
                maleLabel.textColor = UIColor.init(netHex: 0x87B93C)
            case 2:
                femaleImageView.image = UIImage(named: "female_selected")
                femaleLabel.textColor = UIColor.init(netHex: 0x87B93C)
            default:
                maleFemaleImageView.image = UIImage(named: "male_female_selected")
                maleFemaleLabel.textColor = UIColor.init(netHex: 0x87B93C)
            }
        }
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
