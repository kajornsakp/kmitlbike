//
//  MoreViewController.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 5/27/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import UIKit
import SwiftEventBus


enum Section : Int {
    case Header = 0,Gender,Info,Button
}
class MoreViewController : UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    var infokey = ["Email","Phone no."]
    var infoValue : [String] = []
    var buttonValue : [String] = ["About","Logout"]
    var buttonImage : [String] = ["about_button","logout_button"]

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //TODO: use table with more page
        self.infoValue.append(User.getEmail())
        self.infoValue.append(User.getPhoneNum())
        self.tableView.registerNib(UINib.init(nibName: "ProfileHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "headerCell")
        self.tableView.registerNib(UINib.init(nibName: "ProfileButtonTableViewCell", bundle: nil), forCellReuseIdentifier: "buttonCell")
        self.tableView.registerNib(UINib(nibName: "ProfileGenderViewCell",bundle: nil), forCellReuseIdentifier: "genderCell")
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 4
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Section.Header.rawValue:
            return 1
        case Section.Gender.rawValue:
            return 1
        case Section.Info.rawValue :
            return self.infoValue.count
        
        case Section.Button.rawValue:
            return self.infoValue.count
        default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if(section == Section.Header.rawValue){
            return 0
        }
        else{
            return 44
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case Section.Header.rawValue:
            let cell = tableView.dequeueReusableCellWithIdentifier("headerCell") as! ProfileHeaderTableViewCell
            var initials = [Character]()
            initials.append(User.getFirstName().characters.first!)
            initials.append(User.getLastName().characters.first!)
            cell.initialsLabel.text = String(initials)
            cell.firstNameTextField.text = User.getFirstName()
            cell.lastNameTextField.text = User.getLastName()
            return cell
           
        case Section.Gender.rawValue:
            let cell = tableView.dequeueReusableCellWithIdentifier("genderCell") as! ProfileGenderTableViewCell
            cell.genderType = User.gender
            return cell
            
        case Section.Info.rawValue:

            let cell = tableView.dequeueReusableCellWithIdentifier("infoCell") as! ProfileInfoTableViewCell
            cell.profileInfoKeyLabel.text = self.infokey[indexPath.row]
            cell.profileInfoValueLabel.text = self.infoValue[indexPath.row]
            return cell

        case Section.Button.rawValue:
            
                let cell = tableView.dequeueReusableCellWithIdentifier("buttonCell") as! ProfileButtonTableViewCell
                cell.buttonLabel.text = buttonValue[indexPath.row]
                cell.iconImageView.image = UIImage(named: buttonImage[indexPath.row])
                return cell
        default:
            return UITableViewCell(frame: CGRectZero)
        }
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.section {
        case Section.Header.rawValue:
            return 90
        case Section.Gender.rawValue:
            return 44
        case Section.Info.rawValue:
            return 44
        case Section.Button.rawValue:
            return 44
        default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(indexPath.section == Section.Button.rawValue){
            print(indexPath.row)
            switch indexPath.row {
            case 0:
                self.performSegueWithIdentifier("creditSegue", sender: nil)
            case 1:
                logout()
            default:
                break;
            }
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func goToLogin(){
        
        self.view.window!.rootViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func logout(){
        User.clear()
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setObject(false, forKey: "isLogin")
        userDefaults.setObject(false, forKey: "ridingBike")
        userDefaults.setObject("", forKey: "firstName")
        userDefaults.setObject("", forKey: "lastName")
        userDefaults.setObject("", forKey: "token")
        userDefaults.setObject("", forKey: "username")
        goToLogin()
    }
}
