//
//  BikeHistory.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 7/10/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import Foundation
import CoreLocation

class BikeHistory{
    
    var bikeName : String = ""
    var borrow_time : String = ""
    var borrow_date : String = ""
    var total_distance : String = ""
    var total_time : String = ""
    var route_line : [RoutePoint]?

    init(){
        
    }
    
    convenience init(dic : Dictionary<String,AnyObject>){
        self.init()
        self.bikeName = dic["bike_name"] as! String
        self.borrow_time = dic["borrow_time"] as! String
        self.borrow_date = dic["borrow_date"] as! String
        self.total_distance = dic["total_distance"] as! String
        self.total_time = dic["total_time"] as! String
        self.route_line = dic["route_line"] as? [RoutePoint]
    }
    
}
