//
//  StatusServiceManager.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 8/1/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import Foundation
import SwiftEventBus
import Alamofire

class StatusServiceManager {
    
    static let sharedManager = StatusServiceManager()
    
    var url = "\(BASE_URL)/api/v1/user/status/"
    
    func getStatus(){
        let header = ["AUTHORIZATION":User.getToken()]
        Alamofire.request(.GET,url, headers : header).responseJSON{ response in
            
            if(response.result.isSuccess){
                
                let result = response.result.value as! [String:AnyObject]
                let resultresponse = result["result"] as! String
                if(resultresponse == "continue"){
                    BikeMac.setBikeMac(result["bike_mac"] as! String)
                    let current_lat = result["current_lat"] as! String
                    let current_long = result["current_long"] as! String
                    let total_distance = result["total_distance"] as! String
                    let total_time  = result["total_time"] as! String
                    var route_line : [RoutePoint] = []
                    /*
                    if let routeLine = result["route_line"] as! NSArray?{
                        for latlng in routeLine{
                            var latlngDict = latlng as! NSDictionary
                            var lat = latlngDict["lat"] as! Double
                            var lng = latlngDict["lng"] as! Double
                            var time = latlngDict["time"] as! NSTimeInterval
                            var coordinate = RoutePoint(lat: lat, lng: lng, time: time)
                            //TODO : get time from route_line
                            route_line.append(coordinate)
                        }
                    }
                    */
                    let bike = Bike(totaltime: total_time, totaldistance: total_distance, routeLine: route_line)
                    SwiftEventBus.post("onResumeRidingBike", sender: bike)
                }

            }
            
            
        }
    }
}
