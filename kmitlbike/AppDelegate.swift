//
//  AppDelegate.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 5/27/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import UIKit
import GoogleMaps
import Fabric
import Crashlytics
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        Fabric.with([Crashlytics.self])
        
        loadUserData()
        
        LocationService.sharedInstance.startUpdatingLocation()
        GMSServices.provideAPIKey("AIzaSyBQWGJ4MdB4mwOuYVueu_lV0DKZE4CIFik")
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController : UIViewController = mainStoryboard.instantiateInitialViewController()!
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        self.window?.rootViewController = initialViewController
        self.window?.makeKeyAndVisible()
        if (User.getLogin()) {
            initialViewController.performSegueWithIdentifier("bypassLoginSegue", sender: self)
        }
        
        
        
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        //self.saveContext()
        
        LocationService.sharedInstance.stopUpdatingLocation()
    }
    
    func loadUserData(){
        if let firstName = userDefaults.stringForKey("firstName"){
            User.setFirstName(firstName)
        }
        if let lastName = userDefaults.stringForKey("lastName"){
            User.setLastName(lastName)
        }
        if let phoneNum = userDefaults.stringForKey("phoneNum"){
            User.setPhoneNum(phoneNum)
        }
        if let email = userDefaults.stringForKey("email"){
            User.setEmail(email)
        }
        if let token = userDefaults.stringForKey("token"){
            User.setToken(token)
        }
        if let username = userDefaults.stringForKey("username"){
            User.setUsername(username)
        }
        let isLogin = userDefaults.boolForKey("isLogin")
        User.setLogin(isLogin)
        let gender = userDefaults.integerForKey("gender")
        User.setGender(gender)
        let ridingBike = userDefaults.boolForKey("ridingBike")
        User.setRidingBike(ridingBike)
        
    }

}

