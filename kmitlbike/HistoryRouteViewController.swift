//
//  HistoryRouteViewController.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 7/23/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import UIKit
import GoogleMaps

class HistoryRouteViewController: UIViewController,GMSMapViewDelegate {

    var camera = GMSCameraPosition()
    var route :[RoutePoint]?
    
    @IBOutlet var mapView: GMSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initMap()
        sortArray()
        drawMap()
        
        // Do any additional setup after loading the view.
    }
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(true)
        self.mapView.clear()
        self.route = []
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sortArray(){
        
    }
    func initMap(){
        self.camera = GMSCameraPosition.cameraWithLatitude(IC_LATITUTE, longitude: IC_LONGITUTE, zoom: 16)
        self.mapView.camera = camera
        self.mapView.delegate = self
    }

    func drawMap(){
        let path = GMSMutablePath()
        for coordinate in self.route! {
            let latlng = CLLocationCoordinate2D(latitude: coordinate.lat, longitude: coordinate.lng)
            path.addCoordinate(latlng)
        }
        
        let polyline = GMSPolyline(path: path)
        let polylineStroke = GMSPolyline(path: path)
        polylineStroke.strokeWidth = 30
        polyline.strokeWidth = 15
        polylineStroke.strokeColor = UIColor(netHex:0x387cc4)
        polyline.strokeColor = UIColor(netHex: 0x00b3fd)
        polylineStroke.map = self.mapView
        polyline.map = self.mapView
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    

}
