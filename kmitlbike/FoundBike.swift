//
//  FoundBike.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 7/3/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import Foundation
import GoogleMaps
class FoundBike {
    var bike_mac = ""
    var bike_name = ""
    var current_lat : Double = 0
    var current_long : Double = 0
    
    init(bikeMac : String,bikeName : String, currentLat : Double,currentLong : Double){
        self.bike_mac = bikeMac
        self.bike_name = bikeName
        self.current_lat = currentLat
        self.current_long = currentLong
    }
    
    
    func getCoordinate()->CLLocationCoordinate2D{
        return CLLocationCoordinate2D(latitude: self.current_lat, longitude: self.current_long)
    }
    
    func getBikeName()->String{
        return self.bike_name
    }
    
    func getBikeMac()->String{
        return self.bike_mac
    }
}