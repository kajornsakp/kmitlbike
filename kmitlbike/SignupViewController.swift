//
//  SignupViewController.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 6/12/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import UIKit
import SwiftEventBus

enum Sex {
    case Male
    case Female
    case Other
}

class SignUpViewController : UIViewController,UITextFieldDelegate{
    
    var keyboardAppear = false
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var sexSegmentedControl: UISegmentedControl!
    
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)

        
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        SwiftEventBus.onMainThread(self, name: "onSignupSuccess"){
            result in
            print(result)
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        
        SwiftEventBus.onMainThread(self, name: "onSignupFailure"){
            result in
            print(result.object)
            self.signupButton.shake()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func keyboardWillShow(notification: NSNotification) {
        if(!self.keyboardAppear){
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.CGRectValue() {
                self.view.frame.origin.y = -keyboardSize.height
                self.keyboardAppear = true
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if(keyboardAppear){
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.CGRectValue() {
                self.view.frame.origin.y += keyboardSize.height
                self.keyboardAppear = false
            }
        }
    }
    @IBAction func onBackButtonPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func onSignupPressed(sender: AnyObject) {
        if(self.validateField()){
            var gender = 3
            switch self.sexSegmentedControl.selectedSegmentIndex {
            case 0:
                gender = 1
                break
            case 1:
                gender = 2
                break
                
            default:
                gender = 3
                break
                
            }
            var firstName = self.firstNameTextField.text
            var lastName = self.lastNameTextField.text
            var email = self.emailTextField.text
            var mobileNumber = self.mobileNumberTextField.text
            
            SignupServiceManager.getInstance().signup(User.getUsername(), first_name: firstName!, last_name: lastName!, gender: gender, email: email!, phone_no: mobileNumber!)
            
        }
        else{
            self.signupButton.shake()
        }
    }
    
    
    
    func validateField() -> Bool {
        if(self.firstNameTextField.hasText() && self.lastNameTextField.hasText() && self.emailTextField.hasText() && self.mobileNumberTextField.hasText()){
            
            return true
        }
        else{
            return false
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let nameRegex = "^([A-Za-z-])?$"
        let nameTest = NSPredicate(format: "SELF MATCHES %@", nameRegex)
        return nameTest.evaluateWithObject(string)
    }

}