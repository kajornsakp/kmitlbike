//
//  FindServiceManager.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 7/3/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import Foundation
import SwiftEventBus
import Alamofire


class bikeMacName{
    var bikeMac :String = ""
    var bikeName :String = ""
    init(bikemac :String,bikename:String){
        self.bikeMac = bikemac
        self.bikeName = bikename
    }
}

class bikeCodeObject {
    var bikeName : String = ""
    var bikeCode : String = ""
    var bikeMac : String = ""
    init(bikeName : String,bikeCode : String,bikeMac : String){
        self.bikeName = bikeName
        self.bikeCode = bikeCode
        self.bikeMac = bikeMac
    }
}

class FindServiceManager {
    
    static let sharedManager = FindServiceManager()
    
    private var url = BASE_URL+"/api/v1/services/available"
    
    func findAllBike(){
        let header = ["AUTHORIZATION":User.getToken()]
        Alamofire.request(.GET,url, headers : header).responseJSON{ response in
            print("response ::\(response.result)")
            if(response.result.isSuccess){
                let result = response.result.value
                let resultResponse = result!["result"] as! String
                if(resultResponse == "success"){
                    let bikeList = result!["bike_list"] as! NSArray
                    var foundBikeList = [FoundBike]()
                    for bike in bikeList{
                        let bikeName = bike["bike_name"] as! String
                        let bikeMac = bike["bike_mac"] as! String
                        let current_lat = Double(bike["current_lat"] as! String)
                        let current_long = Double(bike["current_long"] as! String)
                        let foundBike = FoundBike(bikeMac: bikeMac, bikeName: bikeName, currentLat: current_lat!, currentLong: current_long!)
                        foundBikeList.append(foundBike)
                    }
                    
                    
                    SwiftEventBus.post("onFindBikeSuccess", sender: foundBikeList)
                }
                else{
                    SwiftEventBus.post("onFindBikeFailure")
                }
                
                
            }else{
                debugPrint("failed to get find bike")
            }
            
            
        }
        
    }

    func getAvailableBikeMac(){
        let header = ["AUTHORIZATION":User.getToken()]
        Alamofire.request(.GET,url, headers : header ).responseJSON{ response in
            print("response ::\(response.result)")
            if(response.result.isSuccess){
                let result = response.result.value
                let resultResponse = result!["result"] as! String
                if(resultResponse == "success"){
                    let bikeList = result!["bike_list"] as! NSArray
                    var foundBikeList = [bikeMacName]()
                    for bike in bikeList{
                        let bikeName = bike["bike_name"] as! String
                        let bikeMac = bike["bike_mac"] as! String
                        foundBikeList.append(bikeMacName(bikemac: bikeMac, bikename: bikeName))
                    }
                    SwiftEventBus.post("onAvailableBike", sender: foundBikeList)
                }
                else{
                    SwiftEventBus.post("onAvailableBikeListFailed",sender: response.result.value)
                }
                
                
            }else{
                debugPrint("failed to get find bike")
            }
            
            
        }
    }
    
    func getBikeCode(completionHandler :([bikeCodeObject]?, NSError?) -> () ){
        let header = ["AUTHORIZATION":User.getToken()]
        Alamofire.request(.GET, url, parameters: nil, encoding: .JSON, headers: header).responseJSON{
            response in
            if(response.result.isSuccess){
                let result = response.result.value as! [String : AnyObject]
                let responseStatus = result["result"] as! String
                if(responseStatus == "success"){
                    let bikeList = result["bike_list"] as! NSArray
                    var foundBikeList = [bikeCodeObject]()
                    for bike in bikeList{
                        let bikeName = bike["bike_name"] as! String
                        let bikeCode = bike["bike_pass"] as! String
                        let bikeMac = bike["bike_mac"] as! String
                        foundBikeList.append(bikeCodeObject(bikeName: bikeName, bikeCode: bikeCode,bikeMac: bikeMac))
                    }
                    completionHandler(foundBikeList,nil)
                }
            }
            else{
                completionHandler(nil,response.result.error)
            }
        }
    }
    
}
