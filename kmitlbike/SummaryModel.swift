//
//  SummaryModel.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 7/2/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import Foundation

class SummaryModel{

    var bikeName : String = ""
    var time : String = ""
    var distance : String = ""
    var borrowDate : String = ""
    var returnDate : String = ""
    
}