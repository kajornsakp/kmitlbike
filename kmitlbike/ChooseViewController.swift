//
//  ChooseViewController.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 6/16/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import UIKit
import CoreBluetooth
import SwiftEventBus
class ChooseViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,CBCentralManagerDelegate,CBPeripheralDelegate {
    
    var centralManager : CBCentralManager?
    var peripherals : Array<CBPeripheral> = Array<CBPeripheral>()
    var currentPeripheral : CBPeripheral?
    var charasteristics = [CBCharacteristic]()
    var rawData : String = ""
    var data :NSData?
    var unlockCharacteristic : CBCharacteristic?
    var bikeMac : String = ""
    var bikeList : [String:String] = [:]
    var activityIndicator = UIActivityIndicatorView()
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Choose a bike"
        let refreshButton = UIBarButtonItem(barButtonSystemItem: .Refresh, target: self, action: #selector(self.refresh))
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        activityIndicator.center = self.view.center
        self.view.addSubview(activityIndicator)
        self.navigationItem.rightBarButtonItem = refreshButton
        self.centralManager = CBCentralManager(delegate: self,queue: nil)
       
        
        SwiftEventBus.onMainThread(self, name: "onBorrowSuccess"){
            notification in
            let key = self.bikeMac.removeColon().sha1().shaToKey()
            let data = key.dataUsingEncoding(NSUTF8StringEncoding)!
            
            self.currentPeripheral?.writeValue(data, forCharacteristic: self.unlockCharacteristic!, type: CBCharacteristicWriteType.WithoutResponse)
            BikeMac.setBikeMac(self.bikeMac)
            self.performSegueWithIdentifier("chooseBike", sender: nil)
        }
        SwiftEventBus.onMainThread(self, name: "onBorrowFailure"){
            notification in
            self.showAlertWithMessage("can't borrow")
        }
        SwiftEventBus.onMainThread(self, name: "onReturnSuccess") {
            notification in
            //self.dismissViewControllerAnimated(true, completion: nil)
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
        
        
        SwiftEventBus.onMainThread(self, name: "onAvailableBike") {
            result in
            var bikeArray = result.object as! [bikeMacName]
            for i in bikeArray{
                self.bikeList[i.bikeMac] = i.bikeName
            }
            self.tableView.reloadData()
        }
    }
    
    func refresh(){
        print("scan")
        self.activityIndicator.startAnimating()
        self.centralManager?.scanForPeripheralsWithServices(nil, options: nil)
    }
    func centralManagerDidUpdateState(central: CBCentralManager) {
        switch central.state {
        case .PoweredOff:
            gotoBluetoothSetting()
            break
        case .PoweredOn:
            print("power on")
            self.activityIndicator.startAnimating()
            self.centralManager?.scanForPeripheralsWithServices(nil, options: nil)
            break
        case .Unauthorized:
            self.showAlertWithMessage("Please authorize bluetooth in setting > KMITL Bike > Bluetooth")
        case .Unsupported:
            self.showAlertWithMessage("This device does not support Bluetooth Low Energy")
        default:
            break
        }
    }
    
    func gotoBluetoothSetting(){
        UIApplication.sharedApplication().openURL(NSURL(string:"prefs:root=Bluetooth")!)
    }
    
    func centralManager(central: CBCentralManager, didDiscoverPeripheral peripheral: CBPeripheral, advertisementData: [String : AnyObject], RSSI: NSNumber) {
       
        if(!self.peripherals.contains(peripheral)){
            print(self.bikeList)
            guard let pname = peripheral.name else{
                return
            }
            if(self.bikeList.keys.contains(pname)){
                self.peripherals.append(peripheral)
                //self.activityIndicator.stopAnimating()
            }
        }
        self.tableView.reloadData()
        
    }
    func peripheral(peripheral: CBPeripheral, didDiscoverServices error: NSError?) {
        if let servicePeripheral = peripheral.services! as [CBService]!{
            for service in servicePeripheral{
                peripheral.discoverCharacteristics(nil, forService: service)
                
            }
        }
    }
    
    func peripheral(peripheral: CBPeripheral, didDiscoverCharacteristicsForService service: CBService, error: NSError?) {
        self.charasteristics = service.characteristics!
        print("characteristics :: \(self.charasteristics)")
        for i in self.charasteristics{
            if(i.UUID.UUIDString == "FFE1"){
                //TODO: Borrow bike
                self.unlockCharacteristic = i
                self.bikeMac = peripheral.name!
                BorrowServiceManager.sharedManager.borrowBike(self.bikeMac)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(self.peripherals.count)
        return self.peripherals.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("bikeCell") as! BikeBluetoothCell
        
        let peripheral = self.peripherals[indexPath.row]
        var bikeMac = peripheral.name!
        cell.bikeNameLabel.text = self.bikeList[bikeMac]
        
        
        
        
    
        
        return cell
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: CGRectZero)
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let peripheral = self.peripherals[indexPath.row]
        peripheral.delegate = self
        currentPeripheral = peripheral
        
        self.centralManager?.connectPeripheral(currentPeripheral!, options:nil)
        
        
        self.centralManager?.stopScan()
    }
    
    func centralManager(central: CBCentralManager, didConnectPeripheral peripheral: CBPeripheral) {
        print("connect to : \(peripheral)")
        peripheral.delegate = self
        peripheral.discoverServices(nil)
        
        
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
       
        getAvailableBike()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        self.centralManager?.stopScan()
        self.peripherals.removeAll()
    }
    func getAvailableBike(){
        FindServiceManager.sharedManager.getAvailableBikeMac()
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "chooseBike" {
            var destinationVC = segue.destinationViewController as! ReturnViewController
            destinationVC.bike = Bike()
        }
    }
    
    
}
