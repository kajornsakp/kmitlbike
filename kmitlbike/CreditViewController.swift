//
//  CreditViewController.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 7/31/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import UIKit


class CreditViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    enum Section : Int {
        case Credit = 0,Library,Asset
    }
    @IBOutlet weak var tableView: UITableView!
    var name = ["Dr. Isara Anantavrasilp","Dr. Ronnachai Tiyarattanachai","Mr. Sasawat Chanate","Mr. Chaiwat Wattanapaiboonsuk","Mr. Puriwat Khantiviriya","Mr. Boonnithi Jiaramaneepinit","Mr. Pavit Noinongyao","Ms. Chatchaya Chanchua","Mr. Kajornsak Peerapathananont"]
    var posiion = ["Adviser","Adviser","Coordinator","Database","Database","Hardware","Hardware","Software (Android) & Database","Software (iOS)"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.name.count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.section {
        case Section.Credit.rawValue:
            return 60
        case Section.Library.rawValue:
            return 60
        case Section.Asset.rawValue:
            return 60
        default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("creditCell") as! CreditTableViewCell
        cell.nameLabel.text = self.name[indexPath.row] as! String
        cell.positionLabel.text = self.posiion[indexPath.row] as! String
        
        return cell
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: CGRectZero)
    }
    
}
