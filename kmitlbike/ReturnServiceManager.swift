//
//  ReturnServiceManager.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 7/3/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//


import Foundation
import SwiftEventBus
import Alamofire
import GoogleMaps
class ReturnServiceManager {
    
    static let sharedManager = ReturnServiceManager()
    
    private var url = "\(BASE_URL)/api/v1/user/return/"
    
    func returnBike(totalTime : String,totalDistance : String,routeLine : [RoutePoint]){
        
        var strRouteLine = ""
        
        for coordinate in routeLine {
            let strObj = "{\"lat\":\(coordinate.lat),\"lng\":\(coordinate.lng),\"time\":\(coordinate.time)},"
            strRouteLine += strObj
        }
        strRouteLine = String(strRouteLine.characters.dropLast())
        
        let header = ["AUTHORIZATION":User.getToken()]
        
        let param = ["total_time":"\(totalTime)","total_distance":"\(totalDistance)","route_line":"[\(strRouteLine)]"]
        
        Alamofire.request(.POST,url, headers : header,parameters : param).responseJSON{ response in
            debugPrint(response.result)
            if(response.result.isSuccess){
                User.setRidingBike(false)
                let userDefaults = NSUserDefaults.standardUserDefaults()
                userDefaults.setObject(false, forKey: "ridingBike")
                SwiftEventBus.post("onReturnSuccess")
                
            }else{
                print(response.result)
                SwiftEventBus.post("onReturnFailure")
            }
            
            
        }
        
    }
    
}