//
//  ShowPassCodeViewController.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 11/9/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import UIKit

protocol PassCodeDelegate: class {
    func didDissmissPasscode(bikeCode : bikeCodeObject)
}
class ShowPassCodeViewController: UIViewController {

    @IBOutlet weak var passCodeHint : UILabel!
    @IBOutlet weak var passCodeLabel : UILabel!
    
    var bikeCode : bikeCodeObject?
    weak var delegate : PassCodeDelegate!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.passCodeLabel.text = bikeCode?.bikeCode
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        self.delegate.didDissmissPasscode(self.bikeCode!)
    }
}
