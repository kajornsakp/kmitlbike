//
//  ProfileInfoTableViewCell.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 7/30/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import UIKit

class ProfileInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var profileInfoKeyLabel: UILabel!
    @IBOutlet weak var profileInfoValueLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
