//
//  ReturnBikeViewController.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 6/21/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import UIKit
import SwiftEventBus


class ReturnBikeViewController : UIViewController{
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
    }
    @IBAction func returnBike(sender: AnyObject) {
        var alert = UIAlertController(title: "Return Bike", message: "Do you really want to return the bike?", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: {
            action in
            self.dismissViewControllerAnimated(true, completion: nil)
            SwiftEventBus.post("onDidReturnBike", sender: "return bike")
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    
}