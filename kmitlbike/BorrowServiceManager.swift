//
//  BorrowServiceManager.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 7/3/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import Foundation
import SwiftEventBus
import Alamofire
class BorrowServiceManager {
    
    static let sharedManager = BorrowServiceManager()
    
    private var url = "\(BASE_URL)/api/v1/user/borrow/"
    
    func borrowBike(bike_mac:String){
        
        let header = ["AUTHORIZATION":User.getToken()]
        let param = ["bike_mac":bike_mac]
        Alamofire.request(.POST,url, headers : header,parameters : param).responseJSON{ response in
            //debugPrint(response.result)
            if(response.result.isSuccess){
                //print(response.result.value)
                User.setRidingBike(true)
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.setObject(true, forKey: "ridingBike")
                SwiftEventBus.post("onBorrowSuccess", sender: "success")
            }else{
             SwiftEventBus.post("onBorrowFailure", sender: "failure")
            }
            
            
        }
        
    }
    func borrowBikeCode(bike_mac:String,completionHandler : (String?,NSError?)->()){
        let header = ["AUTHORIZATION":User.getToken()]
        let param = ["bike_mac":bike_mac]
        Alamofire.request(.POST,url, headers : header,parameters : param).responseJSON{ response in
            //debugPrint(response.result)
            if(response.result.isSuccess){
                //print(response.result.value)
                User.setRidingBike(true)
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.setObject(true, forKey: "ridingBike")
                BikeMac.setBikeMac(bike_mac)
                completionHandler("success",nil)
                //SwiftEventBus.post("onBorrowSuccess", sender: "success")
            }else{
                completionHandler("error", response.result.error)
                //SwiftEventBus.post("onBorrowFailure", sender: "failure")
            }
            
            
        }

    }
    
}
