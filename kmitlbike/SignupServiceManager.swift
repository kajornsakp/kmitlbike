//
//  SignupServiceManager.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 6/7/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import Foundation
import Alamofire
import SwiftEventBus
class SignupServiceManager {
    
    private static var signupServiceManager : SignupServiceManager?
    

    private var url = "\(BASE_URL)/api/v1/services/register/"
    static func getInstance()->SignupServiceManager{
        if(self.signupServiceManager == nil){
            self.signupServiceManager = SignupServiceManager()
        }
        return self.signupServiceManager!
    }
    
    func signup(username:String,first_name:String,last_name:String,gender:Int,email:String,phone_no:String){
        
        Alamofire.request(.POST, url, parameters: ["username": username,"first_name":first_name,"last_name":last_name,"gender":gender,"email":email,"phone_no":phone_no])
            .responseJSON { response in
                print("json response: \(response)")
                
                if(response.result.isSuccess)
                {
                    let result = response.result.value
                    print(result)
                    SwiftEventBus.post("onSignupSuccess",sender: "Success")
                    
                }else{
                    SwiftEventBus.post("onSignupFailure",sender: "No Connection")
                }
                
                
        }
        
    }
    
    
}