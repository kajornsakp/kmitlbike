//
//  User.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 6/4/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import Foundation


//TODO : keep user data locally -> for check in appdelegate

public class User {
    static var token:String? = ""
    static var ridingBike :Bool? = false
    static var isLogin:Bool? = false
    static var username :String? = ""
    static var studentId :String? = ""
    static var gender : Int?
    static var email : String? = ""
    static var firstName : String? = ""
    static var lastName : String? = ""
    static var phoneNum : String? = ""
    
    
    static func setToken(token:String)->Void{
        self.token = token
    }
    
    static func getToken()->String{
        return self.token!
    }
    static func setRidingBike(riding:Bool)->Void{
        self.ridingBike = riding
    }
    static func getRidingBike()->Bool{
        return self.ridingBike!
    }
    
    static func setLogin(login:Bool) -> Void {
        self.isLogin = login
    }
    
    static func getLogin()->Bool{
        return self.isLogin!
    }
    
    static func setUsername(username:String)->Void{
        self.username = username
    }
    
    static func getUsername()->String{
        return self.username!
    }
    
    static func setStudentId(StudentId:String)->Void{
        self.studentId = StudentId
    }
    
    static func getStudentId()->String{
        return self.studentId!
    }
    static func clear()->Void{
        self.setLogin(false)
        self.setToken("")
        self.setRidingBike(false)
        self.setUsername("")
        self.setStudentId("")
        self.setGender(0)
        self.setEmail("")
        self.setFirstName("")
        self.setLastName("")
        self.setPhoneNum("")
    }
    
    static func setGender(gender:Int)->Void{
        self.gender = gender
    }
    
    static func getGender()->Int{
        return self.gender!
    }
    
    static func setEmail(email:String)->Void{
        self.email = email
    }
    
    static func getEmail()->String{
        return self.email!
    }
    
    static func setFirstName(firstName:String)->Void{
        self.firstName = firstName
    }
    
    static func getFirstName()->String{
        return self.firstName!
    }
    
    static func setLastName(lastName:String)->Void{
        self.lastName = lastName
    }
    
    static func getLastName()->String{
        return self.lastName!
    }
    
    static func setPhoneNum(phoneNum:String)->Void{
        self.phoneNum = phoneNum
    }
    
    static func getPhoneNum()->String{
        return self.phoneNum!
    }
    
    
    
    
}
