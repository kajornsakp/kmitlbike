//
//  SummaryViewController.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 6/28/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import UIKit
import GoogleMaps
class SummaryViewController: UIViewController,GMSMapViewDelegate {
    
    var bikeHistory : BikeHistory?
    @IBOutlet weak var bikeNameLabel: UILabel!
    
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var borrowTimeLabel: UILabel!
    
    var camera = GMSCameraPosition()
    @IBOutlet weak var gmsMapView: GMSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bikeNameLabel.text = bikeHistory?.bikeName
        borrowTimeLabel.text = (bikeHistory?.borrow_date)! + " - " + (bikeHistory?.borrow_time)!
        distanceLabel.text = bikeHistory?.total_distance
        durationLabel.text = bikeHistory?.total_time
        
        initMap()
        drawMap()
    }
    
    func initMap(){
        self.camera = GMSCameraPosition.cameraWithLatitude(IC_LATITUTE,
                                                           longitude: IC_LONGITUTE, zoom: 16)
        self.gmsMapView.camera = self.camera
        self.gmsMapView.myLocationEnabled = true
        self.gmsMapView.delegate = self
        
    }
    func drawMap(){
        if self.bikeHistory?.route_line?.count != 0 {
            self.bikeHistory?.route_line?.sortInPlace({ $0.time > $1.time })
            let path = GMSMutablePath()
            var firstRouteLine = self.bikeHistory?.route_line?.first
            var lastRouteLine = self.bikeHistory?.route_line?.last
            var startCoordinate = GMSMarker(position: CLLocationCoordinate2D(latitude: firstRouteLine!.lat, longitude: firstRouteLine!.lng))
            var lastCoordinate = GMSMarker(position: CLLocationCoordinate2D(latitude: lastRouteLine!.lat, longitude: lastRouteLine!.lng))
            
            for i in (self.bikeHistory?.route_line)!{
                path.addCoordinate(CLLocationCoordinate2D(latitude: i.lat, longitude: i.lng))
            }
            
            let polyline = GMSPolyline(path: path)
            let polylineStroke = GMSPolyline(path: path)
            polylineStroke.strokeWidth = 5
            polyline.strokeWidth = 2.5
            polylineStroke.strokeColor = UIColor(netHex:0x387cc4)
            polyline.strokeColor = UIColor(netHex: 0x00b3fd)
            polylineStroke.map = self.gmsMapView
            polyline.map = self.gmsMapView
            polyline.map = self.gmsMapView
            
            startCoordinate.icon = UIImage(named: "bike_marker")
            lastCoordinate.icon = UIImage(named: "goal_marker")
            startCoordinate.map = self.gmsMapView
            lastCoordinate.map = self.gmsMapView
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}