//
//  TutorialVC.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 7/23/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import UIKit

protocol TutorialDelegate : class {
    func didTapButton()
}
class TutorialVC: UIViewController
{
    //MARK: - UIViewController
    weak var delegate:TutorialDelegate?
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    @IBAction func okPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: {
            self.delegate!.didTapButton()
        })
    }
    
    
   
   
   
    
}
