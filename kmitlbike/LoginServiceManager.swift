//
//  LoginServiceManager.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 6/7/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import Foundation
import Alamofire
import SwiftEventBus
class LoginServiceManager {
    
    private static var loginServiceManager : LoginServiceManager?
    
    private var url = "\(BASE_URL)/api/v1/auth/login/"
    static func getInstance()->LoginServiceManager{
        if(self.loginServiceManager == nil){
            self.loginServiceManager = LoginServiceManager()
        }
        return self.loginServiceManager!
    }

    func login(username : String , password : String){
        Alamofire.request(.POST, url, parameters: ["username": username,"password":password])
        .responseJSON { response in
            
            if(response.result.isSuccess)
            {
                let result = response.result.value
                
                switch(result!["result"] as! String){
                case "success":
                    User.setUsername(username)
                    User.setFirstName(result!["first_name"] as! String)
                    User.setLastName(result!["last_name"] as! String)
                    User.setEmail(result!["email"] as! String)
                    User.setGender(result!["gender"] as! Int)
                    User.setPhoneNum(result!["phone_no"] as! String)
                    User.setToken(result!["token"] as! String)
                    
                    User.setLogin(true)
                    let userDefaults = NSUserDefaults.standardUserDefaults()
                    userDefaults.setObject(User.getUsername(), forKey: "username")
                    userDefaults.setObject(User.getFirstName(), forKey: "firstName")
                    userDefaults.setObject(User.getLastName(), forKey: "lastName")
                    userDefaults.setObject(User.getPhoneNum(), forKey: "phoneNum")
                    userDefaults.setObject(User.getGender(), forKey: "gender")
                    userDefaults.setObject(User.getEmail(), forKey: "email")
                    userDefaults.setObject(User.getToken(), forKey: "token")
                    userDefaults.setObject(User.isLogin, forKey: "isLogin")
                    SwiftEventBus.post("onLoginSuccess",sender: "success")
                    
                case "first_time":
                    SwiftEventBus.post("onLoginFirstTime",sender: "success")
                
                case "fail":
                    SwiftEventBus.post("onLoginFailure",sender: "fail")
                    
                case "denied":
                    SwiftEventBus.post("onLoginDenied",sender : "denied")
                    
                default:
                    SwiftEventBus.post("onLoginError",sender : "error")
                }
                
            }else{
                SwiftEventBus.post("onNetworkServiceUnavailable",sender: "No Connection")
            }
            
            
        }
        
    }
}