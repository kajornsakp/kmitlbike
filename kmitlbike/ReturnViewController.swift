//
//  ReturnViewController.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 6/19/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import UIKit
import SwiftEventBus
import CoreLocation

enum State {
    case ButtonVC
    case MapVC
}

class locationObject {
    var location : CLLocationCoordinate2D?
    init(location : CLLocationCoordinate2D){
        self.location = location
    }
}

class RoutePoint{
    var lat : CLLocationDegrees
    var lng : CLLocationDegrees
    var time : NSTimeInterval
    
    init(lat:CLLocationDegrees,lng:CLLocationDegrees,time:NSTimeInterval){
        self.lat = lat
        self.lng = lng
        self.time = time
    }
}

class ReturnViewController: UIViewController,LocationServiceDelegate {
    
    @IBOutlet weak var viewContainer: UIView!
    
    @IBOutlet weak var changeViewButton: UIButton!
    
    @IBOutlet weak var durationValue: UILabel!
    
    @IBOutlet weak var distanceValue: UILabel!
    
    @IBOutlet weak var passCodeLabel : UILabel!
    var buttonVC : ReturnBikeViewController?
    var gmsMapVC : GmsMapViewController?
    var state = State.ButtonVC
    var secTimer = NSTimer()
    var tenSecTimer = NSTimer()
    var bike : Bike!
    var currentLocation : CLLocationCoordinate2D!
    var locationArray : [RoutePoint] = []
    var bikeCode : bikeCodeObject!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initTimer()
        if bikeCode != nil {
            self.passCodeLabel.text = "Code : " + bikeCode.bikeCode
        }
        
        buttonVC = storyboard!.instantiateViewControllerWithIdentifier("returnButtonVC") as! ReturnBikeViewController
        
        gmsMapVC = storyboard!.instantiateViewControllerWithIdentifier("gmsMapVC") as! GmsMapViewController
        self.renderButtonVC()
        self.renderGMSVC()
        LocationService.sharedInstance.delegate = self
        LocationService.sharedInstance.startUpdatingLocation()
        SwiftEventBus.onMainThread(self, name: "onTimerUpdate"){
            notification in
            
            self.durationValue.text = self.bike.getTime()
            
        }
        

        
        SwiftEventBus.onMainThread(self, name: "onLocationUpdateSuccess"){
            notification in
            self.locationArray.removeAll()
        }
        
        SwiftEventBus.onMainThread(self, name: "onLocationUpdateFailure"){
            notification in
        }
        SwiftEventBus.onMainThread(self, name: "onDidReturnBike"){
            result in
            print("return bike")
            ReturnServiceManager.sharedManager.returnBike(self.bike.getTime(), totalDistance: self.bike.totalDistance!, routeLine: self.bike.routeLine!)
            SwiftEventBus.unregister(self)
            self.stopTimer()
        }
        
    }

    override func viewWillAppear(animated: Bool) {
        
//        self.locationArray.append(RoutePoint(lat: (LocationService.sharedInstance.lastLocation?.coordinate.latitude)!, lng: (LocationService.sharedInstance.lastLocation?.coordinate.longitude)!, time: NSDate().timeIntervalSince1970))
    }

 
    func renderButtonVC(){

        self.addChildViewController(buttonVC!)
        buttonVC!.view.frame = CGRectMake(0,0, viewContainer.bounds.size.width,viewContainer.bounds.size.height)
        viewContainer.addSubview(buttonVC!.view)
        buttonVC!.didMoveToParentViewController(self)
    }
    
    func renderGMSVC(){
        self.addChildViewController(gmsMapVC!)
        gmsMapVC!.view.frame = CGRectMake(0, 0, viewContainer.bounds.size.width, viewContainer.bounds.size.height)
        viewContainer.addSubview(gmsMapVC!.view)
        gmsMapVC!.didMoveToParentViewController(self)
        gmsMapVC!.view.hidden = true
        
    }
    
    func initTimer(){
        self.secTimer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(self.counter), userInfo: nil, repeats: true)
        
        self.tenSecTimer = NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector: #selector(self.requestUpdateLocation), userInfo: nil, repeats: true)
        
    }
    
    
    func counter(){
        bike.incrementSecond()
        SwiftEventBus.post("onTimerUpdate")
    }
    
    func updateLocation(){
        // update every three sec
        SwiftEventBus.post("onLocationUpdate")
    }
    
    func requestUpdateLocation(){
        //every ten sec
        
    UpdateUserServiceManager.sharedInstance.updateUserLocation(BikeMac.getBikeMac(), latitude: String(LocationService.sharedInstance.lastLocation!.coordinate.latitude), longitude: String(LocationService.sharedInstance.lastLocation!.coordinate.longitude), total_time: self.bike.getTime(), total_distance: self.bike.totalDistance!, route_line: self.locationArray)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
    }


    @IBAction func swapView(sender: AnyObject) {
        
        if(self.state == State.ButtonVC){
            self.state = .MapVC
            self.buttonVC?.view.hidden = true
            self.gmsMapVC?.view.hidden = false
        }else{
            self.state = .ButtonVC
            self.buttonVC?.view.hidden = false
            self.gmsMapVC?.view.hidden = true
        }
    }
    

    // MARK: LocationService Delegate
    
    func tracingLocation(currentLocation: CLLocation) {
        if(self.currentLocation != nil){
            var lastLocation = CLLocation(latitude: self.currentLocation!.latitude, longitude: self.currentLocation!.longitude)
            var km = (currentLocation.distanceFromLocation(lastLocation)/1000)
            self.bike.distanceKm += km.roundedTwoDigit
            self.updateKm()
        }

        self.currentLocation = currentLocation.coordinate
        gmsMapVC!.onPathUpdate(locationObject(location: self.currentLocation))
        let routePoint  = RoutePoint(lat: self.currentLocation.latitude, lng: self.currentLocation.longitude, time: NSDate().timeIntervalSince1970)

        self.locationArray.append(routePoint)
        self.bike.routeLine!.append(routePoint)

    }
    
    func tracingLocationDidFailWithError(error: NSError) {
        print("failed to tracing \(error)")
    }
    func updateKm(){
        self.bike.totalDistance = String(self.bike.distanceKm)
        self.distanceValue.text = String(self.bike.distanceKm)
    }
    func stopTimer(){
        self.secTimer.invalidate()
        self.tenSecTimer.invalidate()

    }
}



