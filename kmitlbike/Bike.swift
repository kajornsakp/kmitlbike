//
//  Bike.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 7/31/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import Foundation


class BikeMac {
    static var bike_mac : String?
    
    static func setBikeMac(bikeMac: String){
        self.bike_mac = bikeMac
    }
    
    static func getBikeMac()->String{
        return self.bike_mac!
    }
}