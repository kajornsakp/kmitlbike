//
//  UpdateUserServiceManager.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 7/9/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import Foundation
import Alamofire
import CoreLocation
import SwiftEventBus

//00:15:83:00:76:14

class UpdateUserServiceManager {
    static let sharedInstance = UpdateUserServiceManager()
    
    var url = "\(BASE_URL)/api/v1/user/update_user_location/"
    func updateUserLocation(bike_mac : String,latitude:String!,longitude:String!,total_time:String,total_distance :String,route_line :[RoutePoint]){
        let header = ["AUTHORIZATION":User.getToken()]
        var strRoute_line = ""
        if route_line.count != 0 {
            for coordinate in route_line {
                let strObj = "{\"lat\":\(coordinate.lat),\"lng\":\(coordinate.lng),\"time\":\(coordinate.time)},"
                strRoute_line += strObj
            }
            strRoute_line = String(strRoute_line.characters.dropLast())
        }
        print(strRoute_line)
        let param = ["bike_mac":bike_mac as String,"latitude":latitude,"longitude":longitude,"total_time":"\(total_time)","total_distance":total_distance,"route_line":"[\(strRoute_line)]"]

        print("param:\(param)")
        Alamofire.request(.POST, url, parameters: param, headers: header).responseJSON{
            response in
            print(response.result)
            if(response.result.isSuccess){
                SwiftEventBus.post("onLocationUpdateSuccess", sender: "\(response.result.value)")
            }else{
                SwiftEventBus.post("onLocationUpdateFailure", sender: "\(response.result.value)")
            }
        }
 
        
        
    }
}