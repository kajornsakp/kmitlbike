//
//  BikeCodeViewController.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 11/9/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import UIKit

protocol BikeCodeDelegate : class {
    func didTapBikeCode(bikeCode : bikeCodeObject)
}
class BikeCodeViewController: UIViewController {

    var bikeCodeList = [bikeCodeObject](){
        didSet{
            self.tableView.reloadData()
        }
    }
    @IBOutlet weak var tableView : UITableView!
    weak var delegate : BikeCodeDelegate!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView(frame : CGRectZero)
        getBikeCode()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getBikeCode(){
        FindServiceManager.sharedManager.getBikeCode{
            (result,error) in
            if error == nil,let response = result! as [bikeCodeObject]?{
                print(response)
                self.bikeCodeList = response
            }
        }
    }
}

extension BikeCodeViewController : UITableViewDelegate{
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let selectedBike = bikeCodeList[indexPath.row]
        self.dismissViewControllerAnimated(true){
            self.delegate.didTapBikeCode(selectedBike)
        }
    }
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView(frame: CGRectZero)
    }
}

extension BikeCodeViewController : UITableViewDataSource{
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bikeCodeList.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(BikeCodeTableViewCell.identifier) as! BikeCodeTableViewCell
        cell.bikeCode = bikeCodeList[indexPath.row]
        return cell
    }
    
}
