//
//  GmsMapViewController.swift
//  kmitlbike
//
//  Created by Kajornsak Peerapathananont on 6/21/2559 BE.
//  Copyright © 2559 Kajornsak Peerapathananont. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import SwiftEventBus


class GmsMapViewController : UIViewController,GMSMapViewDelegate{

    @IBOutlet weak var mapView: GMSMapView!
    var lastestCoordinate = CLLocationCoordinate2D()
    var camera = GMSCameraPosition()
    var firstDraw : Bool = true
    var count = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        initMap()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        initMap()
    }
    
    func initMap(){
        self.camera = GMSCameraPosition.cameraWithLatitude(IC_LATITUTE,
                                                           longitude: IC_LONGITUTE, zoom: 19)
        self.mapView.camera = self.camera
        self.mapView.myLocationEnabled = true
        self.mapView.delegate = self
        
    }

    func onPathUpdate(locationobject: locationObject) {
        self.count += 1
        let location = locationobject
        if(self.firstDraw){
            self.lastestCoordinate = location.location!
            self.firstDraw = false
            
        }
        else{
            
            let path = GMSMutablePath()
            path.addCoordinate(self.lastestCoordinate)
            path.addCoordinate(location.location!)
            self.lastestCoordinate = location.location!
            let polyline = GMSPolyline(path: path)
            let polylineStroke = GMSPolyline(path: path)
            polylineStroke.strokeWidth = 20
            polyline.strokeWidth = 10
            polylineStroke.strokeColor = UIColor(netHex:0x387cc4)
            polyline.strokeColor = UIColor(netHex: 0x00b3fd)
            polylineStroke.map = self.mapView
            polyline.map = self.mapView
            polyline.map = self.mapView
        }
        self.camera = GMSCameraPosition.cameraWithLatitude(self.lastestCoordinate.latitude,longitude: self.lastestCoordinate.longitude,zoom: 19)
        self.mapView.camera = self.camera
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        SwiftEventBus.unregister(self)
    }
    
    

}
